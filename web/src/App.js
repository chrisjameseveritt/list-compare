import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import CompareList from './CompareList';
import React from 'react';

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list1: {
        name: "List 1",
        list: ""
      },
      list2: {
        name: "List 2",
        list: "foo\nbar\nbaz"
      },
    };
  }

  //onInput={this.props.listNameOnInput}
  handleList1NameChange(e) {
    this.setState({
      list1: {
        name: e.target.value
      }
    })
  }

  render() {
    return (
      <Container>
        <Row className={''}>
          <Col className={'text-center'}>
            <h1>List Compare</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <CompareList listName={this.state.list1.name} list={this.state.list1.list} listNameOnInput={(e) => this.handleList1NameChange(e)} />
          </Col>
          <Col>
            <CompareList listName={this.state.list2.name} list={this.state.list2.list} listNameOnInput={(e) => this.setState({
              list2: {
                name: e.target.value
              }
            })} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;