import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import React from 'react';

export class CompareList extends React.Component {
    constructor(props) {
        super(props);
      }
    render() {
        return (
            <Form>
                <Form.Group className="mb-3" controlId="compareListForm.ListName">
                    <Form.Label>List Name</Form.Label>
                    <Form.Control type="text" defaultValue={this.props.listName} onInput={this.props.listNameOnInput} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="compareListForm.List">
                    <Form.Label>{this.props.listName}</Form.Label>
                    <Form.Control as="textarea" rows={3} defaultValue={this.props.list} />
                </Form.Group>
            </Form>
        );
    }
}

export default CompareList;